

#include <bcm2835.h>
#include <stdio.h>


#define PWM  RPI_GPIO_P1_12
#define M1_1 RPI_GPIO_P1_16
#define M1_2 RPI_GPIO_P1_18
#define M2_1 RPI_GPIO_P1_22
#define M2_2 RPI_GPIO_P1_24

#define PIN_LED RPI_V2_GPIO_P1_07  //RPI_GPIO_P1_03
//#define PIN_SERVO RPI_V2_GPIO_P1_03//RPI_BPLUS_GPIO_J8_37//RPI_V2_GPIO_P1_12



char  i=0, rcv=0;




int main()
{
if (!bcm2835_init())return 1;
unsigned long int runtimeRightLeft=0, runtimeForwardBackward=0;
unsigned int pwm=0;
/*settings pwm*/
bcm2835_gpio_fsel(PWM,BCM2835_GPIO_FSEL_ALT5);
bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_32);
bcm2835_pwm_set_mode(0,1,1);
bcm2835_pwm_set_range(0,1024);
/*settings pin */
bcm2835_gpio_fsel(M1_1, BCM2835_GPIO_FSEL_OUTP);
bcm2835_gpio_write(M1_1, LOW);

/*settings pin */
bcm2835_gpio_fsel(M1_2, BCM2835_GPIO_FSEL_OUTP);
bcm2835_gpio_write(M1_2, LOW);

/*settings pin */
bcm2835_gpio_fsel(M2_1, BCM2835_GPIO_FSEL_OUTP);
bcm2835_gpio_write(M2_1, LOW);

/*settings pin */
bcm2835_gpio_fsel(M2_2, BCM2835_GPIO_FSEL_OUTP);
bcm2835_gpio_write(M2_2, LOW);


/*settings pin for light driver*/
bcm2835_gpio_fsel(PIN_LED, BCM2835_GPIO_FSEL_OUTP);
bcm2835_gpio_write(PIN_LED, LOW);

/*settings pin for control serv*/
//bcm2835_gpio_fsel(PIN_SERVO, BCM2835_GPIO_FSEL_OUTP);
//bcm2835_gpio_write(PIN_SERVO, LOW);



//bcm2835_gpio_fsel(PIN_SERVO, BCM2835_GPIO_FSEL_ALT5);
//bcm2835_pwm_set_clock()


printf ("Welcome to the control raspberry's robot\n");
printf ("Please enter time working commands in ms for reverse and forward\n");
printf("reverse time=");
scanf("%d",&runtimeRightLeft);
printf("forward time=");
scanf("%d",&runtimeForwardBackward);
printf("speed(pwm,max 1024)=");
scanf("%d",&pwm);
bcm2835_pwm_set_data(0,pwm);
while(1)
{
printf ("Write command (wasd or exit):");
scanf("%c",&i);
switch(i)
{
case 's':
{
bcm2835_gpio_write(M1_1, HIGH);
bcm2835_gpio_write(M1_2, LOW);
bcm2835_gpio_write(M2_1, HIGH);
bcm2835_gpio_write(M2_2, LOW);
delay(runtimeForwardBackward);
bcm2835_gpio_write(M1_1, LOW);
bcm2835_gpio_write(M1_2, LOW);
bcm2835_gpio_write(M2_1, LOW);
bcm2835_gpio_write(M2_2, LOW);
break;
}

case 'w':
{
bcm2835_gpio_write(M1_1, LOW);
bcm2835_gpio_write(M1_2, HIGH);
bcm2835_gpio_write(M2_1, LOW);
bcm2835_gpio_write(M2_2, HIGH);
delay(runtimeForwardBackward);
bcm2835_gpio_write(M1_1, LOW);
bcm2835_gpio_write(M1_2, LOW);
bcm2835_gpio_write(M2_1, LOW);
bcm2835_gpio_write(M2_2, LOW);
break;
}
case 'a':
{
bcm2835_gpio_write(M1_1, LOW);
bcm2835_gpio_write(M1_2, HIGH);
bcm2835_gpio_write(M2_1, HIGH);
bcm2835_gpio_write(M2_2, LOW);
delay(runtimeRightLeft);
bcm2835_gpio_write(M1_1, LOW);
bcm2835_gpio_write(M1_2, LOW);
bcm2835_gpio_write(M2_1, LOW);
bcm2835_gpio_write(M2_2, LOW);
break;
}
case 'd':
{
bcm2835_gpio_write(M1_1, HIGH);
bcm2835_gpio_write(M1_2, LOW);
bcm2835_gpio_write(M2_1, LOW);
bcm2835_gpio_write(M2_2, HIGH);
delay(runtimeRightLeft);
bcm2835_gpio_write(M1_1, LOW);
bcm2835_gpio_write(M1_2, LOW);
bcm2835_gpio_write(M2_1, LOW);
bcm2835_gpio_write(M2_2, LOW);
break;
}
case 'l':
{
bcm2835_gpio_write(PIN_LED, HIGH);
break;
}
case 'o':
{
bcm2835_gpio_write(PIN_LED, LOW);
break;
}

default :
{
break;
}
}

if (i=='e') return 1;

}
}
